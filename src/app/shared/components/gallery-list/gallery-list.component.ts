import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gallery-list',
  templateUrl: './gallery-list.component.html',
  styleUrls: ['./gallery-list.component.scss']
})
export class GalleryListComponent implements OnInit {

  constructor() { }

  @Input() locationList: any[] = [];
  ngOnInit(): void {
    // console.log(this.locationList)
  }

}
