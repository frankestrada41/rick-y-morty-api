import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from 'src/app/shared/service/characters.service';

@Component({
  selector: 'app-characters-detail-page',
  templateUrl: './characters-detail-page.component.html',
  styleUrls: ['./characters-detail-page.component.scss']
})
export class CharactersDetailPageComponent implements OnInit {

  characterP: any;

  constructor(private root: ActivatedRoute, private charactersService: CharactersService) { }

  ngOnInit(): void {
    this.root.paramMap.subscribe((params) => {
      console.log(params.get('idCharacter'))
      const characterId = params.get('idCharacter')
      this.charactersService.getCharacter(characterId).subscribe((character) => {
        console.log(character)
        this.characterP = character
      })
    })
  }
}
