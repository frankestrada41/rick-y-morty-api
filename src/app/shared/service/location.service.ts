import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    constructor(private http: HttpClient) { }



    getLocation() {

        return this.http.get('https://rickandmortyapi.com/api/location');
    }
    getLocationDetails(idLocation: any) {

        return this.http.get('https://rickandmortyapi.com/api/location/' + idLocation);
    }

}