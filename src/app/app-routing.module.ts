import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { LocationPageComponent } from './pages/location-page/location-page.component';
import { CharactersDetailPageComponent } from './pages/characters-page/pages/characters-detail-page/characters-detail-page.component';
import { LocationDetailPageComponent } from './pages/location-page/pages/location-detail-page/location-detail-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'characters', component: CharactersPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'location', component: LocationPageComponent },
  { path: 'location/:idLocation', component: LocationDetailPageComponent },
  { path: 'characters/:idCharacter', component: CharactersDetailPageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
