import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/app/shared/service/location.service';


@Component({
  selector: 'app-location-page',
  templateUrl: './location-page.component.html',
  styleUrls: ['./location-page.component.scss']
})
export class LocationPageComponent implements OnInit {

  locations: any[] = [];

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.locationService.getLocation().subscribe((locationData: any) => {
      console.log(locationData.results)
      this.locations = locationData.results
    })
  }

}
