import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationService } from 'src/app/shared/service/location.service';

@Component({
  selector: 'app-location-detail-page',
  templateUrl: './location-detail-page.component.html',
  styleUrls: ['./location-detail-page.component.scss']
})
export class LocationDetailPageComponent implements OnInit {

  locationP: any;
  constructor(private root: ActivatedRoute, private locationService: LocationService) { }

  ngOnInit(): void {
    this.root.paramMap.subscribe((params) => {
      console.log(params.get('idLocation'))
      const locationId = params.get('idLocation')
      this.locationService.getLocationDetails(locationId).subscribe((locData) => {
        console.log(locData)
        this.locationP = locData

      })
    })

  }



}
